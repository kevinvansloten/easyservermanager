package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ServerRunnable implements Runnable
{
    public Process p;
    public Server Server;

    public ServerRunnable(Server server)
    {
        this.Server = server;
    }

    @Override
    public void run(){
		File file = new File(Server.Path);
		ProcessBuilder pb = new ProcessBuilder("java", "-Xmx"+Server.Ram+"m", "-jar", Server.Path, "nogui");
		pb.directory(file.getParentFile());

		try {
            p = pb.start();
            BufferedReader in = new BufferedReader(
                                new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = in.readLine()) != null){
                System.out.println(line);
                Server.serverConsole.println(line);
			}
		} catch(Exception e){
			System.out.println(e);
		}
    }

    public boolean isProcessRunning()
    {
        return p.isAlive();
    }

    public void forceStop(){
        p.destroy();
    }

    public void sendCommand(String command)
    {
        BufferedWriter streamInput = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
        try 
        {
            streamInput.write(command+"\n");
            streamInput.flush();
        } 
        catch (IOException e) 
        {
            
        }
    }

}