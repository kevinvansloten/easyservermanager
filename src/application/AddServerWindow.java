package application;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddServerWindow {

    public JFrame addServerFrame;

    public AddServerWindow(){

    }

    public void open(){
        addServerFrame = new JFrame();
        addServerFrame.setLayout(new GridBagLayout());
        addServerFrame.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = c.weighty = 0;
        addServerFrame.setVisible(true);
        addServerFrame.setSize(300,300);
        addServerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JLabel nameLabel = new JLabel("Server Name:");
        JLabel pathLabel = new JLabel("Server Path:");
        JLabel ramLabel = new JLabel("Ram: (MB)");

        JTextField nameField = new JTextField();
        JTextField pathField =  new JTextField();
        JTextField ramField = new JTextField();

        JButton browseButton =  new JButton("Browse");
        JButton cancelButton = new JButton("Cancel");
        JButton saveButton = new JButton("Save");

        browseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final JFileChooser fc = new JFileChooser();
                int returnVal = fc.showOpenDialog(addServerFrame);
                if(returnVal == JFileChooser.APPROVE_OPTION){
                    pathField.setText(fc.getSelectedFile().toPath().toString());
                }
			}
        });

        cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
                addServerFrame.dispose();
            }
        });

        saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
                addServer(nameField.getText(), pathField.getText(), addServerFrame, ramField.getText());
            }
        });

        c.weighty = 3;
        c.insets = new Insets(20,0,20,0);  //top padding
        c.gridy = 0;
        c.gridx = 0;
        c.gridwidth = 1;
        addServerFrame.add(nameLabel, c);

        c.gridx = 1;
        c.gridwidth = 3;
        addServerFrame.add(nameField, c);

        c.gridy = 2;
        c.gridx = 0;
        c.gridwidth = 1;
        addServerFrame.add(ramLabel, c);

        c.gridx = 2;
        c.gridwidth = 1;
        addServerFrame.add(ramField, c);

        c.gridy = 1;
        c.gridx = 0;
        addServerFrame.add(pathLabel, c);

        
        c.gridy = 1;
        c.gridx = 1;
        c.gridheight = 1;
        c.gridwidth = 2;
        c.weightx = 9;
		c.weighty = 4;
        addServerFrame.add(pathField, c);

        c.gridx = 3;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weighty = 4;
        c.weightx = 1;
        addServerFrame.add(browseButton, c);
       
        c.insets = new Insets(20,10,20,10);
        c.gridy = 3;
        c.gridx = 0;
        c.gridwidth = 2;
        addServerFrame.add(cancelButton, c);

        c.gridx = 2;
        addServerFrame.add(saveButton, c);
    }

    public void addServer(String name, String path, JFrame frame, String ram){
        if(validateInput(name, path, ram)){
            MainWindow.addServer(name, path, Integer.parseInt(ram));
            frame.dispose();
        }
        else
        {
            System.out.print("Error, invalid info");
        }
    }

    public boolean validateInput(String name, String path, String ram){
        if(new File(path).exists() && getFileExtension(new File(path)).equals(new String(".jar"))){
            try {
                if(Integer.parseInt(ram) >= 512){
                    return true;
                }
            }
            catch (Exception e) {
            
            }
            JOptionPane.showMessageDialog(addServerFrame ,"Ram has to be a number bigger than 512");
            return false;
        }
        JOptionPane.showMessageDialog(addServerFrame ,"The selected file is not a valid Jar file!");
        return false;
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        String extension = name.substring(lastIndexOf);
        return extension;
    }
    
}
