package application;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TextField;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.text.DefaultCaret;
import javax.swing.JOptionPane;

public class MainWindow {

	public static ArrayList<Server> servers = new ArrayList<Server>();
	public static MainWindow window;

	JTabbedPane tp;
	AddServerWindow addServerWindow;
	JFrame mainWindow;

	private static Image logo;
    private static SystemTray sysTray;
    private static PopupMenu menu;
	private static MenuItem exitItem;
	private static MenuItem restoreItem;
    private static TrayIcon trayIcon;

	public static void main(String[] args) {
		servers = JSONReader.ReadServers(); // Get the saved server configs

		// TODO Version 0.1 move code to program.JAVA
		try {
			window = new MainWindow();
			window.open();

			logo  = new ImageIcon("./logo.jpg").getImage();

			window.mainWindow.setIconImage(logo);
			window.mainWindow.setTitle("Easy Server Manager");

			if (SystemTray.isSupported()) {
				sysTray = SystemTray.getSystemTray();	
	
				// Create popupmenu
				menu = new PopupMenu();
	
				// Create items
				exitItem = new MenuItem("Exit");
				restoreItem = new MenuItem("Restore window");
	
				// Add items to menu
				menu.add(restoreItem);
				menu.add(exitItem);
	
				// Add action listener to the item in the popup menu
				exitItem.addActionListener(new ActionListener() {
				   public void actionPerformed(ActionEvent e) {
					   window.exitProcedure();
				   }
				});

				restoreItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						window.mainWindow.setVisible(true);
					}
				 });
				
				// Create system tray icon.
				trayIcon = new TrayIcon(logo, "Easy Server Manager", menu);
	
				// Add the tray icon to the system tray.
				try {
					sysTray.add(trayIcon);
					}
				catch(AWTException e) {
				   System.out.println(e.getMessage());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void open() {
		mainWindow = new JFrame();
		tp = new JTabbedPane();

		// Create a tab for each server in the array servers
		if(servers != null){
			for (Server server : servers) {
				addToWindow(server);
			}
		}

		mainWindow.getContentPane().add(tp);

		JButton NewServerButton = new JButton("Add server");
		NewServerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(addServerWindow != null){
					addServerWindow.addServerFrame.dispose(); //Dispose possible existing windows
				}
				addServerWindow = new AddServerWindow();
				addServerWindow.open();
			}
		});
		mainWindow.getContentPane().add(NewServerButton, BorderLayout.SOUTH);
		mainWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainWindow.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				window.mainWindow.setVisible(false);
			}
		});
		mainWindow.setSize(750, 600);
		mainWindow.setVisible(true);

	}

	public void exitProcedure() 
	{
		// Check if a server is still running
		for (Server server : servers) {
			if(server.isRunning()){
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog(null, "One or more servers are still running, are you sure you want to quit?", "Warning", dialogButton);
				
				if(dialogResult == JOptionPane.NO_OPTION){
					return;
				}

				break;
			}
		}

		// Run code to shutdown all servers
		for (Server server : servers) {
			server.stop();
		}

		// TODO Version 0.2 Wait for servers to stop

		//Exit the entire process
		System.exit(0);
	}

	public void addToWindow(Server server) {
		JPanel jp = new JPanel(); // TODO Version 0.1 Place this in a different class for better organized code
			
			ServerConsole consoleLog = new ServerConsole(5, 10);
			jp.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
			jp.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.BOTH;
			c.weightx = c.weighty = 0;

			// Create a Start button
			c.gridx = 0;
			c.gridy = 0;
			c.gridwidth = 2;
			c.gridheight = 1;
			c.weightx = 5;
			c.weighty = 1;
			JButton StartButton = new JButton("Start Server");
			StartButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					server.start();
				}
			});
			jp.add(StartButton, c);
			

			// Create a Stop button
			c.gridx = 2;
			c.gridy = 0;
			c.gridwidth = 2;
			c.gridheight = 1;
			JButton StopButton = new JButton("Stop Server");
			StopButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					server.stop();
				}
			});
			jp.add(StopButton, c);
			
			
			// Add a Console Text field
			c.gridx = 0;
			c.gridy = 1;
			c.gridwidth = 4;
			c.weightx = 10;
			c.weighty = 9;
			DefaultCaret caret = (DefaultCaret)consoleLog.getCaret();
			caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
			consoleLog.setEditable(false);
			server.setServerconsole(consoleLog);
			consoleLog.setText(server.Name);
			jp.add(new JScrollPane(consoleLog), c);

			// Add a Console input Text field
			c.gridheight = 1;
			c.weighty = 1;
			c.gridx = 0;
			c.gridy = 3;
			c.gridwidth = 3;
			TextField commandInput = new TextField();
			commandInput.setColumns(1);
			Font f = new Font("Arial", Font.BOLD, 20);
			commandInput.setFont(f);
			jp.add(commandInput, c);

			c.gridx = 3;
			c.gridwidth = 1;
			c.weightx = 2;
			JButton CommandSendButton = new JButton("Send");
			CommandSendButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(server.sendCommand(commandInput.getText())) {
						commandInput.setText("");
					}else {
						consoleLog.println("Server not running!");
					}
				}
			});
			
			Action action = new AbstractAction()
			{
			    @Override
			    public void actionPerformed(ActionEvent e)
			    {
			        CommandSendButton.doClick();
			    }
			};
			commandInput.addActionListener( action );
			
			jp.add(CommandSendButton, c);
			
			// Add delete button
			c.gridx = 0;
			c.gridwidth = 4;
			c.gridheight = 1;
			c.gridy = 4;
			JButton DeleteButton = new JButton("Delete");
			DeleteButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					tp.remove(jp);
					servers.remove(server);
					JSONReader.SaveServers(servers);
				}
			});
			jp.add(DeleteButton, c);

			tp.add(server.Name, jp); //Add the Panel 	
		}

	public static void addServer(String name, String path, int ram){
		Server newServer = new Server(name, path, ram);
		if(servers == null){
			servers = new ArrayList<Server>();
		}
		servers.add(newServer);
		window.addToWindow(newServer);
		JSONReader.SaveServers(servers);
	}

}
