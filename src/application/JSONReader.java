package application;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.json.*;

public abstract class JSONReader {

    public static ArrayList<Server> servers;
    public static boolean notFoundException = false;

    public static ArrayList<Server> ReadServers() { // TODO Version 0.1 clean up code
        String path;
        try {
            path = new File(JSONReader.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
        } catch (URISyntaxException e) {
            path = "no";
            e.printStackTrace();
        }
        if (new File(path + "/servers.json").exists()) {
            try {
                File serverFile = new File(path + "/servers.json");
                String Json = "";
                try {
                    BufferedReader br = new BufferedReader(new FileReader(serverFile));
                    String st;
                    while ((st = br.readLine()) != null) {
                        Json += st;
                    }
                    br.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                JSONObject obj = new JSONObject(Json);
                JSONArray arr = obj.getJSONArray("servers");
                servers = new ArrayList<Server>();

                for (int i = 0; i < arr.length(); i++) {
                    try{
                        String serverName = arr.getJSONObject(i).getString("name");
                        String serverPath = arr.getJSONObject(i).getString("path");
                        int serverRam = Integer.parseInt(arr.getJSONObject(i).getString("ram"));
                        if(new File(serverPath).exists()){
                            Server newServer = new Server(serverName, serverPath, serverRam);
                            servers.add(newServer);
                        }
                        else{
                            notFoundException = true;
                        }
                    } catch(Exception e) {
                        System.out.println(e + " This is probably due to an empty server list in the JSON file, nothing to worry about");
                    }
            
            }
        } 
        catch(JSONException e){
            System.out.println("Error, Invalid JSON, Regenerating");
            SaveServers(new ArrayList<Server>());
        }
        }
        else{
            SaveServers(new ArrayList<Server>());
        }
        if(notFoundException){
            JOptionPane.showMessageDialog(null, "Some servers could not be found and have been removed from the list.");
            SaveServers(servers);
        }
        return servers;
    }

    public static void SaveServers(ArrayList<Server> serverlist) {
        String path;
        try {
            path = new File(JSONReader.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
        } catch (URISyntaxException e) {
            path = "no";
            e.printStackTrace();
        }
        File file = new File(path + "/servers.json");
        if (file.exists()) {
            file.delete();
        }
        try {
            PrintWriter out = new PrintWriter(file);
            out.println("{\"softwareVersion\":{ \"version\": 0.1 }, \"servers\":[ ");
            int count = 0;
            String slash = "\\";
            String escapedSlash = slash+slash;
            String twoEscapedSlashes = escapedSlash+escapedSlash;
            for (Server server : serverlist) 
            { 
                String serverPath = server.Path;
                serverPath = serverPath.replaceAll(escapedSlash, twoEscapedSlashes) ;
                count++;
                out.println("{\"name\": \"" + server.Name + "\", \"path\": \"" + serverPath + "\", \"ram\": \"" + server.Ram + "\" }");
                if(serverlist.size() != count){
                    out.println(",");
                }
            }
            out.println("] }");
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void SaveSettings(){
        //TODO Version 0.2 implement general program settings
    }

    public static void ReadSettings(){
        //TODO Version 0.2 implement general program settings
    }

}