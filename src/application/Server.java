package application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	public String Name;
	public String Path;
	public int Ram;
	public ServerConsole serverConsole;
	public ServerRunnable runnable;
	public ExecutorService executorService;
	
	public Server(String name, String path, int ram) {
		this.Name = name;
		this.Path = path;
		this.Ram = ram;	
	}
	
	public void start() 
	{
		if(!isRunning())
		{
			serverConsole.setText("");
			serverConsole.println("Starting minecraft server...");
			runnable = new ServerRunnable(this);
			executorService = Executors.newSingleThreadExecutor();
			executorService.execute(runnable);
		} 
		else
		{
			if(!runnable.isProcessRunning())
			{
				// Restart the runnable to prevent bugs
				stop();
				start();
				return;
			}
			serverConsole.println("Server already running...");
		}
	}
	
	public void stop() {
		if(isRunning())
		{
			// TODO Version 0.2 Send command stop to server instead of forcestop
			runnable.forceStop();
			executorService.shutdown();
			executorService.shutdownNow();
			serverConsole.println("Server stopped");
		}
	}

	public boolean isRunning(){
		if(executorService != null && !executorService.isShutdown()){
			return true;
		}
		return false;
	}

	public void setServerconsole(ServerConsole console){
		serverConsole = console;
	}

	public boolean sendCommand(String command){
		if(runnable != null) {
			runnable.sendCommand(command);
			return true;
		}
		return false;
	}
	
	public void Delete() {
		
	}
}
