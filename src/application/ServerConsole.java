package application;

import javax.swing.JTextArea;

public class ServerConsole extends JTextArea{

    private static final long serialVersionUID = 1;

    public ServerConsole(int x, int y){
       super(x, y);
    } 

    public void println(String line){
        this.setText(this.getText() + System.lineSeparator() + line);
    }
}